package com.example.sejin.callbus;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Geocoder;
import android.os.Build;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;

public class DestActivity extends AppCompatActivity implements OnMapReadyCallback {
    ArrayList<Marker> markers, clones;
    ArrayList<MarkerOptions> optionses;
    Button commit;
    TextView address;
    Double destLat, destLon;
    String destName;
    ActionBar actionBar;
    Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dest);


        if (Build.VERSION.SDK_INT >= 21) {
            getSupportActionBar().setElevation(0);
        }

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle("도착지 설정");

        address = (TextView) findViewById(R.id.address);
        commit = findViewById(R.id.commit);


        destName = getIntent().getStringExtra("name");

        if (destName != null)
            Manager.Dest = destName;

        destLat = getIntent().getDoubleExtra("lat", 0);
        destLon = getIntent().getDoubleExtra("lon", 0);

        if (destName == null) address.setText("출발지: " + Manager.Dest);
        else address.setText("출발지: " + destName);
        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        geocoder = new Geocoder(this);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                NavUtils.navigateUpFromSameTask(this);
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(destLat, destLon),13 );
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(37.5985300, 127.0914880), 15);
        googleMap.moveCamera(cameraUpdate);


        LatLng latLng1 = new LatLng(37.5985300, 127.0914880);
        MarkerOptions markerOptions1 = new MarkerOptions();
        markerOptions1.position(latLng1);
        markerOptions1.title("홈플러스 상봉점");
//        markerOptions1.snippet("1번 위치 입니다.");
        markerOptions1.icon(BitmapDescriptorFactory
                .fromResource(R.drawable.gray_pin));

        LatLng latLng2 = new LatLng(37.5968710, 127.0902710);
        MarkerOptions markerOptions2 = new MarkerOptions();
        markerOptions2.position(latLng2);
        markerOptions2.title("코스트코 코리아 상봉점");
//        markerOptions2.snippet("2번 위치 입니다.");
        markerOptions2.icon(BitmapDescriptorFactory
                .fromResource(R.drawable.gray_pin));

        LatLng latLng3 = new LatLng(37.5964780, 127.0935960);
        MarkerOptions markerOptions3 = new MarkerOptions();
        markerOptions3.position(latLng3);
        markerOptions3.title("이마트 상봉점");
//        markerOptions3.snippet("3번 위치 입니다.");
        markerOptions3.icon(BitmapDescriptorFactory
                .fromResource(R.drawable.gray_pin));

        LatLng latLng4 = new LatLng(37.5992550, 127.0965400);
        MarkerOptions markerOptions4 = new MarkerOptions();
        markerOptions4.position(latLng4);
        markerOptions4.title("롯데하이마트 상봉점");
//        markerOptions4.snippet("4번 위치 입니다.");
        markerOptions4.icon(BitmapDescriptorFactory
                .fromResource(R.drawable.gray_pin));

        LatLng latLng5 = new LatLng(37.5993660, 127.0968460);
        final MarkerOptions markerOptions0 = new MarkerOptions();
        markerOptions0.position(latLng5);
        markerOptions0.title("LG 베스트샵 중랑점");
//        markerOptions5.snippet("5번 위치 입니다.");
        markerOptions0.icon(BitmapDescriptorFactory
                .fromResource(R.drawable.gray_pin));

        markers = new ArrayList<Marker>();
        markers.add(googleMap.addMarker(markerOptions0));
        markers.add(googleMap.addMarker(markerOptions1));
        markers.add(googleMap.addMarker(markerOptions2));
        markers.add(googleMap.addMarker(markerOptions3));
        markers.add(googleMap.addMarker(markerOptions4));

        optionses = new ArrayList<MarkerOptions>();
        optionses.add(markerOptions0);
        optionses.add(markerOptions1);
        optionses.add(markerOptions2);
        optionses.add(markerOptions3);
        optionses.add(markerOptions4);


        clones = new ArrayList<Marker>();
        clones = (ArrayList<Marker>) markers.clone();


        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                int num = 0;

                for (int i = 0; i < markers.size(); i++) {
                    if (marker.getTitle().equals(markers.get(i).getTitle())) {
                        num = i;
                    }
                    googleMap.clear();
                }

                for (int i = 0; i < markers.size(); i++) {
                    if (i != num) {
                        googleMap.addMarker(optionses.get(i));
                    } else {
                        MarkerOptions op = new MarkerOptions();
                        op.position(optionses.get(num).getPosition());
                        op.title(optionses.get(num).getTitle());
                        op.icon(BitmapDescriptorFactory.fromResource(R.drawable.red_pin));


                        googleMap.addMarker(op).showInfoWindow();
                    }

                    commit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            AlertDialog.Builder alert_confirm = new AlertDialog.Builder(DestActivity.this);
                            alert_confirm.setMessage("도착지를 정확히 설정하셨습니까?").setCancelable(true).setPositiveButton("확인",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            Intent intent = new Intent(getApplicationContext(), ReserveActivity.class);

                                            //                                                intent.putExtra("dest", geocoder.getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude, 1).get(0).getAddressLine(0));
                                            intent.putExtra("dest", marker.getTitle());
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);


                                        }
                                    }).setNegativeButton("취소",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // 'No'
                                            return;
                                        }
                                    });
                            AlertDialog alert = alert_confirm.create();
                            alert.show();


                        }
                    });

                }
                return true;
            }
        });
    }
}

//    @Override
//    public void onInfoWindowClick(final Marker marker) {
//
//        marker.setIcon (BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
//        commit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent =new Intent(getApplicationContext(), ReserveActivity.class);
//
//
//                try {
//                    intent.putExtra("dest", geocoder.getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude,1).get(0).getAddressLine(0));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                startActivity(intent);
//            }
//        });
//
//        commit.setTextColor(Color.YELLOW);
//    }
