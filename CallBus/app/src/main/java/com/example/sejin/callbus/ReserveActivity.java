package com.example.sejin.callbus;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReserveActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    final int DIALOG_DATE = 1;
    final int DIALOG_TIME = 2;
    Button commit;
    RelativeLayout layout1, layout2;
    TextView tv1, tv2;

    private boolean isDateSet = false;
    private boolean isTimeSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve);
        setTitle("예약 시간");

        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);


        if (Build.VERSION.SDK_INT >= 21) {
            getSupportActionBar().setElevation(0);
        }

        layout1 = findViewById(R.id.lay1);
        layout2 = findViewById(R.id.lay2);


        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar sunday, saturday;
                sunday = Calendar.getInstance();
                List<Calendar> weekends = new ArrayList<>();
                int weeks = 20;

                for (int i = 0; i < (weeks * 7); i = i + 7) {
                    sunday = Calendar.getInstance();
                    sunday.add(Calendar.DAY_OF_YEAR, (Calendar.SUNDAY - sunday.get(Calendar.DAY_OF_WEEK) + 7 + i));
                    saturday = Calendar.getInstance();
                    saturday.add(Calendar.DAY_OF_YEAR, (Calendar.SATURDAY - saturday.get(Calendar.DAY_OF_WEEK) + i));
                    weekends.add(saturday);
                    weekends.add(sunday);
                }


                Calendar christmas;
                christmas = Calendar.getInstance();
                christmas.set(Calendar.YEAR, 2017);
                christmas.set(Calendar.MONTH, Calendar.DECEMBER);
                christmas.set(Calendar.DATE, 25);
                weekends.add(christmas);

                Calendar chooseok;
                for (int i = 2; i<10; i++){
                    chooseok= Calendar.getInstance();
                    chooseok.set(Calendar.YEAR, 2017);
                    chooseok.set(Calendar.MONTH, Calendar.OCTOBER);
                    chooseok.set(Calendar.DATE, i) ;
                    weekends.add(chooseok);
                }



                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        ReserveActivity.this
//                        ,
//                        sunday.get(Calendar.YEAR),
//                        sunday.get(Calendar.MONTH),
//                        sunday.get(Calendar.DAY_OF_MONTH)
                );
                Calendar[] disabledDays = weekends.toArray(new Calendar[weekends.size()]);
                dpd.setDisabledDays(disabledDays);

                dpd.show(getFragmentManager(), "날짜를 선택하세요!");
            }
        });
        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();

                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        ReserveActivity.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        true
                );


                tpd.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                    }
                });

                List<Timepoint> arr = new ArrayList<Timepoint>();
//                arr.add(new Timepoint(10));
//                arr.add(new Timepoint(11));
//                arr.add(new Timepoint(12));
//                arr.add(new Timepoint(13));
//                arr.add(new Timepoint(14));
//                arr.add(new Timepoint(15));
//                arr.add(new Timepoint(16));
//                arr.add(new Timepoint(17));
//                arr.add(new Timepoint(18));
//
//
//
//                tpd.setSelectableTimes(arr.toArray(new Timepoint[arr.size()]));
                tpd.setMinTime(10, 0, 0);
                tpd.setMaxTime(18, 0, 0);
                tpd.show(getFragmentManager(), "시간을 선택하세요!");


            }
        });

        final View dialogView = View.inflate(this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        commit = findViewById(R.id.commit);

        commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDateSet && isTimeSet) {
                    Intent intent = new Intent(ReserveActivity.this, CompleteActvity.class);
                    intent.putExtra("date", tv1.getText().toString());
                    intent.putExtra("time", tv2.getText().toString());
                    intent.putExtra("dest", getIntent().getStringExtra("dest"));
                    startActivity(intent);
                } else {
                    Toast.makeText(ReserveActivity.this, "예약 정보를 확인하세요", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                NavUtils.navigateUpFromSameTask(this);
            }
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        String date = year + "년 " + (monthOfYear + 1) + "월 " + dayOfMonth + "일";
        tv1.setText(date);
        isDateSet = true;

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String time;

        if (hourOfDay > 12) {
            time = "오후 " + (hourOfDay - 12) + "시 " + minute + "분";
        } else {
            time = "오전 " + hourOfDay + "시 " + minute + "분";
        }

        isTimeSet = true;
        tv2.setText(time);
    }


//
//    @Override
//    @Deprecated
//    protected Dialog onCreateDialog(int id) {
//        switch (id) {
//            case DIALOG_DATE:
//                DatePickerDialog dpd = new DatePickerDialog
//                        (ReserveActivity.this, // 현재화면의 제어권자
//                                new DatePickerDialog.OnDateSetListener() {
//                                    public void onDateSet(DatePicker view,
//                                                          int year, int monthOfYear, int dayOfMonth) {
//
//                                        tv2.setText(year + "년 " + (monthOfYear + 1) + "월 " + dayOfMonth + "일 ");
//
//                                    }
//                                }
//                                , // 사용자가 날짜설정 후 다이얼로그 빠져나올때
//                                //    호출할 리스너 등록
//                                2017, 9, 26); // 기본값 연월일
//                return dpd;
//            case DIALOG_TIME:
//
//
////                BoundTimePickerDialog tpd =
////                        new BoundTimePickerDialog(ReserveActivity.this,
////                                new TimePickerDialog.OnTimeSetListener() {
////                                    @Override
////                                    public void onTimeSet(TimePicker view,
////                                                          int hourOfDay, int minute) {
////
////                                        tv3.setText(hourOfDay + "시 " + minute + "분 ");
////
////                                    }
////                                }, // 값설정시 호출될 리스너 등록
////                                4, 19, true); // 기본값 시분 등록
////
////
//////                TimePickerDialog tpd =
//////                        new TimePickerDialog(ReserveActivity.this,
//////                                new TimePickerDialog.OnTimeSetListener() {
//////                                    @Override
//////                                    public void onTimeSet(TimePicker view,
//////                                                          int hourOfDay, int minute) {
//////
//////                                        tv3.setText(hourOfDay + "시 " + minute + "분 ");
//////
//////                                    }
//////                                }, // 값설정시 호출될 리스너 등록
//////                                4, 19, false); // 기본값 시분 등록
////                // true : 24 시간(0~23) 표시
////                // false : 오전/오후 항목이 생김
////                tpd.setMax(18,0);
////                tpd.setMin(10,0);
//                return tpd;
////        }
//
//
//            return super.onCreateDialog(id);
//        }
//
//
//    }
}
