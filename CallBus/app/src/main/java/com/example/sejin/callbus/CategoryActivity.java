package com.example.sejin.callbus;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

public class CategoryActivity extends AppCompatActivity implements View.OnClickListener {

    ImageButton login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 21){
            getSupportActionBar().setElevation(0);
        }
        setContentView(R.layout.activity_category);
        setTitle("관심 제품");
        login = findViewById(R.id.login);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(CategoryActivity.this, StartActivity.class);
        startActivity(intent);
    }
}
